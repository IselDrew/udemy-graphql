const express = require('express');
const { graphqlHTTP } = require('express-graphql');
const schema = require('./schema/schema');
// const expressGraphQL = require('express-graphql'); // must be this way is deprecated

const app = express();

const PORT = 4000;

app.use(
  '/graphql',
  //   expressGraphQL({ // deprecated
  graphqlHTTP({
    schema,
    graphiql: true,
  })
);

app.listen(PORT, () => {
  console.log(`Listening on port: ${PORT}`);
});
